package com.remotegen.app.screens.review

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.view.View
import com.andrognito.flashbar.Flashbar
import com.jakewharton.rxbinding2.view.RxView
import com.remotegen.app.R
import com.remotegen.app.extensions.showErrorSnackBarWithAction
import com.remotegen.app.extensions.showFlashBarMessage
import com.remotegen.app.extensions.toLiveData
import com.remotegen.app.screens.BaseActivity
import com.remotegen.app.screens.ViewModelFactory
import com.remotegen.app.screens.entities.Result
import com.remotegen.app.screens.list.PlaceListActivity
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.ErrorMessageFactory
import com.zeyad.usecases.api.DataServiceFactory
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.functions.Function7
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.content_review.*

class ReviewActivity : BaseActivity<ReviewState, ReviewVM>() {

    private var eventObservable: Observable<BaseEvent<*>> = Observable.empty()
    private val postOnResumeEvents = PublishSubject.create<BaseEvent<*>>()

    override fun initialize() {
        eventObservable = Observable.empty()
        viewModel = ViewModelProviders.of(this,
                ViewModelFactory(DataServiceFactory.getInstance()!!, getString(R.string.place_api_key)))
                .get(ReviewVM::class.java)
        if (viewState == null) {
            viewState = ReviewState(intent.getParcelableExtra(UI_MODEL), false)
        }
    }

    override fun setupUI(isNew: Boolean) {
        setContentView(R.layout.activity_review)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Review ${viewState.result.name}"
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorFont))
        eventObservable = eventObservable.mergeWith(RxView.clicks(fab_rate)
                .map {
                    ReviewEvent(ratingBar.numStars, 1, 2, 3, 3, 1,
                            2, 1, et_review.text.toString())
                })
        Observable.combineLatest(RxView.clicks(bt_wifi).map { true },
                RxView.clicks(bt_plugs).map { true },
                RxView.clicks(bt_light).map { true },
                RxView.clicks(bt_noise).map { true },
                RxView.clicks(bt_ambiance).map { true },
                RxView.clicks(bt_comfort).map { true },
                RxView.clicks(bt_price).map { true },
                Function7<Boolean, Boolean, Boolean, Boolean, Boolean, Boolean, Boolean, Boolean>
                { wifi, plugs, light, noise, ambiance,
                  comfort, price ->
                    wifi && plugs && light && noise && ambiance && comfort && price
                }).toFlowable(BackpressureStrategy.BUFFER).toLiveData()
                .observe(this, Observer { fab_rate.isEnabled = it!! })

        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, _ ->
            when (ratingBar.numStars) {
                1 -> tv_rating_label.text = "Horrible"
                2 -> tv_rating_label.text = "Bad"
                3 -> tv_rating_label.text = "Ok"
                4 -> tv_rating_label.text = "Quite Good"
                else -> tv_rating_label.text = "Excellent"
            }
        }
        tv_title.text = getString(R.string.rate, viewState.result.name)
    }

    override fun toggleViews(toggle: Boolean) {
        loaderLayout.bringToFront()
        loaderLayout.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    override fun errorMessageFactory() = ErrorMessageFactory { it.localizedMessage }

    override fun showError(message: String) {
        fab_rate.showErrorSnackBarWithAction(message, "retry", View.OnClickListener {
            postOnResumeEvents.onNext(ReviewEvent(ratingBar.numStars, 1, 2, 3,
                    3, 1, 2, 1, et_review.text.toString()))
        })
    }

    override fun renderSuccessState(successState: ReviewState) {
        if (successState.done) {
            showFlashBarMessage("Review Confirmed!!")
                    .backgroundColorRes(R.color.green)
                    .primaryActionText("Continue")
                    .primaryActionTextColor(R.color.white)
                    .primaryActionTapListener(object : Flashbar.OnActionTapListener {
                        override fun onActionTapped(bar: Flashbar) {
                            bar.dismiss()
                            startActivity(PlaceListActivity.getCallingIntent(this@ReviewActivity))
                        }
                    })
                    .show()
        }
    }

    override fun events() = eventObservable.mergeWith(postOnResumeEvents)!!

    companion object {
        fun getCallingIntent(context: Context, result: Result): Intent {
            return Intent(context, ReviewActivity::class.java).putExtra(UI_MODEL, result)
        }
    }
}
