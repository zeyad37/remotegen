package com.remotegen.app.screens.list

import android.location.Location
import android.support.v7.util.DiffUtil
import android.util.Pair
import com.google.android.gms.tasks.Task
import com.remotegen.app.Constants.SEARCH
import com.remotegen.app.R
import com.remotegen.app.screens.entities.PlaceList
import com.remotegen.app.screens.entities.ResultsItem
import com.remotegen.app.screens.list.events.GetLastLocation
import com.zeyad.gadapter.ItemInfo
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.BaseViewModel
import com.zeyad.rxredux.core.redux.StateReducer
import com.zeyad.usecases.api.IDataService
import com.zeyad.usecases.requests.GetRequest
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.functions.Function

/**
 * @author ZIaDo on 4/29/18.
 */
class PlaceVM(private val dataUseCase: IDataService, private val apiKey: String) : BaseViewModel<PlaceListSate>() {

    override fun init(vararg objects: Any) {
    }

    override fun stateReducer(): StateReducer<PlaceListSate> {
        return StateReducer { newResult, event, currentStateBundle ->
            val currentItemInfo = currentStateBundle.placeList.toMutableList()
            val pair = Flowable.fromIterable<ResultsItem>((newResult as PlaceList).results!!)
                    .map { resultsItem -> ItemInfo(resultsItem, R.layout.place_item_layout) }
                    .toList()
                    .toFlowable()
                    .scan<Pair<MutableList<ItemInfo>, DiffUtil.DiffResult>>(Pair(currentItemInfo,
                            DiffUtil.calculateDiff(PlaceDiffCallback(mutableListOf(), mutableListOf()))))
                    { pair1, next ->
                        Pair(next, DiffUtil.calculateDiff(PlaceDiffCallback(pair1.first, next)))
                    }
                    .skip(1)
                    .blockingFirst()
            PlaceListSate(pair.first, pair.second)
        }
    }

    override fun mapEventsToActions(): Function<BaseEvent<*>, Flowable<*>> {
        return Function { baseEvent -> getPlaces((baseEvent as GetLastLocation).payLoad) }
    }

    private fun getPlaces(task: Task<Location>) =
            getLastLocation(task).flatMap { location ->
                getPlacesByLatLng(location.latitude.toString(),
                        location.longitude.toString())
            }

    private fun getLastLocation(task: Task<Location>): Flowable<Location> {
        return Flowable.create({ emitter ->
            task.addOnFailureListener { emitter.onError(it) }
                    .addOnSuccessListener { location ->
                        if (location != null)
                        //                                        emitter.onNext(new Location(""));
                            emitter.onNext(location)
                    }
        }, BackpressureStrategy.LATEST)
    }

    private fun getPlacesByLatLng(lat: String, lng: String): Flowable<PlaceList> {
        return dataUseCase.getObject(GetRequest.Builder(PlaceList::class.java, false)
                .fullUrl(SEARCH + String.format("location=%s,%s&radius=%s&type=%s&key=%s",
                        lat, lng, //"41.4", "2.15",
                        RADIUS, TYPE, apiKey))
                .build())
    }

    companion object {
        private val TYPE = "cafe"
        private val RADIUS = "750"
    }
}
