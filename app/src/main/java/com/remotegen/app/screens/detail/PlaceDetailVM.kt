package com.remotegen.app.screens.detail

import com.remotegen.app.Constants.DETAIL
import com.remotegen.app.screens.entities.PlaceDetail
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.BaseViewModel
import com.zeyad.rxredux.core.redux.StateReducer
import com.zeyad.usecases.api.IDataService
import com.zeyad.usecases.requests.GetRequest

import io.reactivex.Flowable
import io.reactivex.functions.Function

/**
 * @author ZIaDo on 4/29/18.
 */
class PlaceDetailVM(private val dataUseCase: IDataService, private val apiKey: String) : BaseViewModel<PlaceDetailState>() {

    override fun init(vararg objects: Any) {}

    override fun stateReducer() =
            StateReducer<PlaceDetailState> { newResult, s, current ->
                PlaceDetailState(false, "", (newResult as PlaceDetail).result, current.photoRef)
            }


    override fun mapEventsToActions(): Function<BaseEvent<*>, Flowable<*>> =
            Function { baseEvent -> getPlaceById(baseEvent.payLoad as String) }


    private fun getPlaceById(id: String): Flowable<PlaceDetail> {
        return dataUseCase.getObject(GetRequest.Builder(PlaceDetail::class.java, false)
                .fullUrl(DETAIL + String.format("placeid=%s&key=%s", id, apiKey))
                .build())
    }
}
