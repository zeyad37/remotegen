package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Northeast(@field:SerializedName("lng")
                     val lng: Double? = null,
                     @field:SerializedName("lat")
                     val lat: Double? = null) : Parcelable