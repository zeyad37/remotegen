package com.remotegen.app.screens.detail

import com.zeyad.rxredux.core.redux.BaseEvent

/**
 * @author ZIaDo on 4/9/18.
 */
internal class GetPlaceEvent(val placeId: String) : BaseEvent<String> {

    override fun getPayLoad() = placeId
}