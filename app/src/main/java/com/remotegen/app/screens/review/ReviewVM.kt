package com.remotegen.app.screens.review

import com.remotegen.app.screens.entities.Result
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.BaseViewModel
import com.zeyad.rxredux.core.redux.StateReducer
import com.zeyad.usecases.api.IDataService
import io.reactivex.Flowable
import io.reactivex.functions.Function

class ReviewVM(private val dataUseCase: IDataService) : BaseViewModel<ReviewState>() {
    override fun init(vararg p0: Any?) {
    }

    override fun stateReducer(): StateReducer<ReviewState> {
        return StateReducer { newResult, event, reviewState ->
            ReviewState(Result(), newResult as Boolean)
        }
    }

    override fun mapEventsToActions(): Function<BaseEvent<Any>, Flowable<*>> {
        return Function { event -> submitReview(event.payLoad as Map<String, Any>) }
    }

    private fun submitReview(payload: Map<String, Any>) =
            Flowable.just(true)
//            dataUseCase.postObject<Boolean>(PostRequest.Builder(Map::class.java, false)
//                    .payLoad(payload)
//                    .responseType(Boolean::class.java)
//                    .url("")
//                    .build())!!
}
