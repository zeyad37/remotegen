package com.remotegen.app.screens.list

import android.os.Parcel
import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.zeyad.gadapter.ItemInfo

/**
 * @author ZIaDo on 3/20/18.
 */
data class PlaceListSate(val placeList: List<ItemInfo>,
                         var diffCallback: DiffUtil.DiffResult = DiffUtil.calculateDiff
                         (PlaceDiffCallback(mutableListOf<ItemInfo>(), mutableListOf<ItemInfo>()))) : Parcelable {

    constructor(parcel: Parcel) : this(emptyList())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<PlaceListSate> {
        override fun createFromParcel(parcel: Parcel) = PlaceListSate(parcel)

        override fun newArray(size: Int) = arrayOfNulls<PlaceListSate?>(size)
    }
}
