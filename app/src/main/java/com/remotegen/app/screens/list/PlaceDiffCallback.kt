package com.remotegen.app.screens.list

import android.support.v7.util.DiffUtil
import com.remotegen.app.screens.entities.ResultsItem
import com.zeyad.gadapter.ItemInfo

/**
 * @author ZIaDo on 4/24/18.
 */

class PlaceDiffCallback(val oldList: MutableList<ItemInfo>, val newList: MutableList<ItemInfo>) : DiffUtil.Callback() {
    /**
     * Called by the DiffUtil to decide whether two object represent the same Item.
     *
     *
     * For example, if your items have unique ids, this method should check their id equality.
     *
     * @param oldItemPosition The position of the item in the old list
     * @param newItemPosition The position of the item in the new list
     * @return True if the two items represent the same object or false if they are different.
     */
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].getData<ResultsItem>().id == newList[newItemPosition].getData<ResultsItem>().id

    /**
     * Returns the size of the old list.
     *
     * @return The size of the old list.
     */
    override fun getOldListSize() = oldList.size

    /**
     * Returns the size of the new list.
     *
     * @return The size of the new list.
     */
    override fun getNewListSize() = newList.size

    /**
     * Called by the DiffUtil when it wants to check whether two items have the same data.
     * DiffUtil uses this information to detect if the contents of an item has changed.
     *
     *
     * DiffUtil uses this method to check equality instead of [Object.equals]
     * so that you can change its behavior depending on your UI.
     * For example, if you are using DiffUtil with a
     * [RecyclerView.Adapter][android.support.v7.widget.RecyclerView.Adapter], you should
     * return whether the items' visual representations are the same.
     *
     *
     * This method is called only if [.areItemsTheSame] returns
     * `true` for these items.
     *
     * @param oldItemPosition The position of the item in the old list
     * @param newItemPosition The position of the item in the new list which replaces the
     * oldItem
     * @return True if the contents of the items are the same or false if they are different.
     */
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val new = newList[newItemPosition].getData<ResultsItem>()
        val old = oldList[oldItemPosition].getData<ResultsItem>()
        val isIdTheSame = new.id == old.id
        val isNameTheSame = new.name == old.name
        val isVicinityTheSame = new.vicinity == old.vicinity
        val isPlaceIdTheSame = new.placeId == old.placeId
        return isIdTheSame && isNameTheSame && isVicinityTheSame && isPlaceIdTheSame
    }

//    /**
//     * When [.areItemsTheSame] returns `true` for two items and
//     * [.areContentsTheSame] returns false for them, DiffUtil
//     * calls this method to get a payload about the change.
//     *
//     *
//     * For example, if you are using DiffUtil with [RecyclerView], you can return the
//     * particular field that changed in the item and your
//     * [ItemAnimator][android.support.v7.widget.RecyclerView.ItemAnimator] can use that
//     * information to run the correct animation.
//     *
//     *
//     * Default implementation returns `null`.
//     *
//     * @param oldItemPosition The position of the item in the old list
//     * @param newItemPosition The position of the item in the new list
//     *
//     * @return A payload object that represents the change between the two items.
//     */
//    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
//        val new = newList[newItemPosition].getData<ResultsItem>()
//        val old = oldList[oldItemPosition].getData<ResultsItem>()
//        val set = mutableSetOf<String>()
//        val isIdTheSame = new.id == old.id
//        val isNameTheSame = new.name == old.name
//        val isVicinityTheSame = new.vicinity == old.vicinity
//        val isPlaceIdTheSame = new.placeId == old.placeId
//        if (isIdTheSame.not()) {
////            set.add(ID)
//        }
//        if (isNameTheSame.not()) {
////            set.add(HOURS)
//        }
//        if (isVicinityTheSame.not()) {
////            set.add(MINUTES)
//        }
//        if (isPlaceIdTheSame.not()) {
////            set.add(SECONDS)
//        }
//        return set
//    }
}