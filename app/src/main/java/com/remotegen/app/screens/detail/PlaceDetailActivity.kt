package com.remotegen.app.screens.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Pair
import android.view.MenuItem
import android.view.View
import com.remotegen.app.R
import com.remotegen.app.screens.list.PlaceListActivity
import com.zeyad.rxredux.core.redux.BaseActivity.UI_MODEL
import kotlinx.android.synthetic.main.activity_place_detail.*

/**
 * An activity representing a single Place detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [PlaceListActivity].
 */
class PlaceDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_detail)
        setSupportActionBar(detail_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        collapsing_toolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.colorFont))
        detail_toolbar.title = ""
        if (savedInstanceState == null) {
            addFragment(R.id.place_detail_container,
                    PlaceDetailFragment.newInstance(intent.getParcelableExtra(UI_MODEL)),
                    "", null)
        }
    }

    private fun addFragment(containerViewId: Int, fragment: Fragment, currentFragTag: String?,
                            sharedElements: List<Pair<View, String>>?) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (sharedElements != null) {
            for (pair in sharedElements) {
                fragmentTransaction.addSharedElement(pair.first, pair.second)
            }
        }
        if (currentFragTag == null || currentFragTag.isEmpty()) {
            fragmentTransaction.addToBackStack(fragment.tag)
        } else {
            fragmentTransaction.addToBackStack(currentFragTag)
        }
        fragmentTransaction.add(containerViewId, fragment, fragment.tag).commit()
    }

    override fun onBackPressed() {
        //        navigateUpTo(new Intent(this, UserListActivity.class));
        supportFinishAfterTransition() // exit animation
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, PlaceListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    companion object {
        fun getCallingIntent(context: Context, placeDetailState: PlaceDetailState): Intent {
            return Intent(context, PlaceDetailActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(UI_MODEL, placeDetailState)
        }
    }
}