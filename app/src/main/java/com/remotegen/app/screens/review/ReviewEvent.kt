package com.remotegen.app.screens.review

import com.zeyad.rxredux.core.redux.BaseEvent

/**
 * @author ZIaDo on 6/18/18.
 */
class ReviewEvent(private val stars: Int, private val wifi: Int, private val plugs: Int,
                  private val light: Int, private val noise: Int, private val ambiance: Int,
                  private val comfort: Int, private val price: Int, private val review: String) : BaseEvent<Map<String, Any>> {
    override fun getPayLoad(): Map<String, Any> {
        return hashMapOf("Stars" to stars, "Wifi" to wifi, "Plugs" to plugs, "Light" to
                light, "Noise" to noise, "Ambiance" to ambiance, "Comfort" to comfort, "Price" to
                price, "Review" to review)
    }
}