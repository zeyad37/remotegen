package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Viewport(@field:SerializedName("southwest")
                    val southwest: Southwest? = null,
                    @field:SerializedName("northeast")
                    val northeast: Northeast? = null) : Parcelable