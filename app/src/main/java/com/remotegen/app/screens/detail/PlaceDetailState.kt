package com.remotegen.app.screens.detail

import android.os.Parcelable
import com.remotegen.app.R
import com.remotegen.app.screens.entities.Result
import com.zeyad.gadapter.ItemInfo
import io.reactivex.Flowable
import kotlinx.android.parcel.Parcelize

/**
 * @author ZIaDo on 4/9/18.
 */
@Parcelize
data class PlaceDetailState(var isTwoPane: Boolean = false,
                            var placeId: String = "",
                            var place: Result? = null, val photoRef: String = "") : Parcelable {
    fun getAdapterList(): MutableList<ItemInfo> = Flowable.fromIterable((place?.reviews?.toMutableList()))
            .map { resultsItem -> ItemInfo(resultsItem, R.layout.review_item_layout) }
            .toList()
            .blockingGet()
}