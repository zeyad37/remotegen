package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Geometry(@field:SerializedName("location")
                    val location: Location? = null,
                    @field:SerializedName("viewport")
                    val viewport: Viewport) : Parcelable