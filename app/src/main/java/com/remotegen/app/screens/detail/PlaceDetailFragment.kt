package com.remotegen.app.screens.detail

import android.app.Activity.RESULT_OK
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.remotegen.app.Constants
import com.remotegen.app.Constants.IS_SIGNED_IN
import com.remotegen.app.Constants.RC_SIGN_IN
import com.remotegen.app.Constants.USER_NAME
import com.remotegen.app.R
import com.remotegen.app.extensions.hasM
import com.remotegen.app.extensions.showFlashBarMessage
import com.remotegen.app.screens.BaseFragment
import com.remotegen.app.screens.ViewModelFactory
import com.remotegen.app.screens.list.PlaceListActivity
import com.remotegen.app.screens.list.viewholders.EmptyViewHolder
import com.remotegen.app.screens.review.ReviewActivity
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyad.rxredux.core.redux.BaseActivity.UI_MODEL
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.ErrorMessageFactory
import com.zeyad.usecases.api.DataServiceFactory
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_place_detail.*
import kotlinx.android.synthetic.main.activity_place_list.*
import kotlinx.android.synthetic.main.place_detail.*
import java.util.*


/**
 * A fragment representing a single Place detail screen.
 * This fragment is either contained in a [PlaceListActivity]
 * in two-pane mode (on tablets) or a [PlaceDetailActivity]
 * on handsets.
 */
class PlaceDetailFragment : BaseFragment<PlaceDetailState, PlaceDetailVM>() {

    private var eventObservable: Observable<BaseEvent<*>> = Observable.empty()
    private var wasReviewing: Boolean = false

    private lateinit var reviewsAdapter: GenericRecyclerViewAdapter

    private val sharedPrefs: SharedPreferences by lazy {
        context?.getSharedPreferences("", Context.MODE_PRIVATE)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            postponeEnterTransition()
        }
        if (hasM()) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
        //        setSharedElementReturnTransition(null); // supply the correct element for return transition
    }

    override fun errorMessageFactory() = ErrorMessageFactory { it.localizedMessage }

    override fun initialize() {
        viewModel = ViewModelProviders.of(this,
                ViewModelFactory(DataServiceFactory.getInstance()!!, getString(R.string.place_api_key)))
                .get(PlaceDetailVM::class.java)
        eventObservable = Observable
                .just(GetPlaceEvent(arguments?.getParcelable<PlaceDetailState>(UI_MODEL)?.placeId!!))
        viewState = PlaceDetailState()
    }

    override fun events(): Observable<BaseEvent<*>> {
        return eventObservable
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.place_detail, container, false)
    }

    /**
     * Called immediately after [.onCreateView]
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     * @param view The View returned by [.onCreateView].
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as PlaceDetailActivity).fab_review.setOnClickListener {
            if (sharedPrefs.getBoolean(IS_SIGNED_IN, false))
                startActivity(ReviewActivity.getCallingIntent(context!!, viewState.place!!))
            else startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(Arrays.asList(AuthUI.IdpConfig.EmailBuilder().build(),
                            AuthUI.IdpConfig.PhoneBuilder().build(),
                            AuthUI.IdpConfig.GoogleBuilder().build()))
                    .build(), RC_SIGN_IN)
        }
        (activity as PlaceDetailActivity).fab_book.setOnClickListener {
            if (sharedPrefs.getBoolean(IS_SIGNED_IN, false))
                (activity as PlaceDetailActivity).showFlashBarMessage("Booking Confirmed!!")
                        .backgroundColorRes(R.color.green)
                        .show()
            else startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(Arrays.asList(AuthUI.IdpConfig.EmailBuilder().build(),
                            AuthUI.IdpConfig.PhoneBuilder().build(),
                            AuthUI.IdpConfig.GoogleBuilder().build()))
                    .build(), RC_SIGN_IN)
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        reviewsAdapter = object : GenericRecyclerViewAdapter(
                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericRecyclerViewAdapter.GenericViewHolder<*> {
                return when (viewType) {
                    R.layout.empty_view -> EmptyViewHolder(layoutInflater
                            .inflate(R.layout.empty_view, parent, false))
                    R.layout.review_item_layout -> ReviewViewHolder(layoutInflater
                            .inflate(R.layout.review_item_layout, parent, false))
                    else -> throw IllegalArgumentException("Could not find view of type $viewType")
                }
            }
        }
        reviewsAdapter.setAreItemsClickable(true)
        rv_reviews.layoutManager = LinearLayoutManager(activity)
        rv_reviews.adapter = reviewsAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser
                with(sharedPrefs.edit()) {
                    putBoolean(IS_SIGNED_IN, true)
                    putString(USER_NAME, user?.displayName)
                    apply()
                }
                if (!wasReviewing) {
                    startActivity(ReviewActivity.getCallingIntent(context!!, viewState.place!!))
                } else {
                    (activity as PlaceDetailActivity).showFlashBarMessage("Booking Confirmed!!").show()
                }
            } else {
                when (response?.error?.errorCode) {
                    400 -> {
                    }
                    else -> {
                    }
                }
            }
        }
    }

    override fun renderSuccessState(successState: PlaceDetailState) {
        val place = successState.place
        (activity as PlaceDetailActivity).let { placeDetailActivity ->
            placeDetailActivity.detail_toolbar.title = place?.name
            placeDetailActivity.iv_place_detail.viewTreeObserver.addOnPreDrawListener {
                Glide.with(this@PlaceDetailFragment)
                        .load(Constants.PHOTO + successState.photoRef +
                                "&key=${getString(R.string.place_api_key)}&maxwidth=${placeDetailActivity.iv_place_detail.measuredWidth}")
                        .into(placeDetailActivity.iv_place_detail)
                true
            }
//            Glide.with(context!!)
//                    .load(place?.icon)
//                    .apply(RequestOptions.centerCropTransform())
//                    .into(placeDetailActivity.iv_place_detail)
        }
        reviewsAdapter.dataList = successState.getAdapterList()
        reviewsAdapter.notifyDataSetChanged()
        tv_address.text = place?.formattedAddress
        tv_phone.text = place?.internationalPhoneNumber
        tv_rating.text = place?.rating.toString()
    }

    internal fun glideRequestListenerCore(): Boolean {
        activity.also { it?.supportStartPostponedEnterTransition() }
        return false
    }

    override fun toggleViews(toggle: Boolean) {
        loadLayout.bringToFront()
        loadLayout.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    override fun showError(message: String) {
        showErrorSnackBar(message, loaderLayout, Snackbar.LENGTH_LONG)
    }

    companion object {
        fun newInstance(placeDetailState: PlaceDetailState): PlaceDetailFragment {
            val userDetailFragment = PlaceDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(UI_MODEL, placeDetailState)
            userDetailFragment.arguments = bundle
            return userDetailFragment
        }
    }
}
