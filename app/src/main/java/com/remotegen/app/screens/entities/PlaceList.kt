package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceList(
//        @field:SerializedName("html_attributions")
//                     val htmlAttributions: List<Any>? = null,
        @field:SerializedName("results")
        val results: List<ResultsItem?>? = null,
        @field:SerializedName("status")
        val status: String? = null) : Parcelable