package com.remotegen.app.screens.list

import android.Manifest
import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.app.Dialog
import android.app.SearchManager
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.LocationServices
import com.remotegen.app.Constants.FIRED
import com.remotegen.app.R
import com.remotegen.app.extensions.hasLollipop
import com.remotegen.app.extensions.showErrorSnackBarWithAction
import com.remotegen.app.screens.BaseActivity
import com.remotegen.app.screens.ViewModelFactory
import com.remotegen.app.screens.detail.PlaceDetailActivity
import com.remotegen.app.screens.detail.PlaceDetailFragment
import com.remotegen.app.screens.detail.PlaceDetailState
import com.remotegen.app.screens.entities.ResultsItem
import com.remotegen.app.screens.list.events.GetLastLocation
import com.remotegen.app.screens.list.viewholders.EmptyViewHolder
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyad.rxredux.core.redux.BaseEvent
import com.zeyad.rxredux.core.redux.ErrorMessageFactory
import com.zeyad.usecases.api.DataServiceFactory
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.abc_alert_dialog_material.*
import kotlinx.android.synthetic.main.activity_place_list.*
import kotlinx.android.synthetic.main.place_item_layout.view.*
import kotlinx.android.synthetic.main.place_list.*

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [PlaceDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class PlaceListActivity : BaseActivity<PlaceListSate, PlaceVM>() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private var currentFragTag: String? = null

    private val postOnResumeEvents = PublishSubject.create<BaseEvent<*>>()
    private var eventObservable: Observable<BaseEvent<*>> = Observable.empty()

    private lateinit var placesAdapter: GenericRecyclerViewAdapter

    override fun errorMessageFactory() = ErrorMessageFactory { it.localizedMessage }

    override fun initialize() {
        eventObservable = Observable.empty()
        viewModel = ViewModelProviders.of(this,
                ViewModelFactory(DataServiceFactory.getInstance()!!, getString(R.string.place_api_key)))
                .get(PlaceVM::class.java)
        if (viewState == null || viewState?.placeList?.isEmpty()!!) {
            viewState = viewState ?: PlaceListSate(emptyList())
            val googleApiAvailability = GoogleApiAvailability.getInstance()
            val dialog = googleApiAvailability.getErrorDialog(this, googleApiAvailability
                    .isGooglePlayServicesAvailable(this), 1)
            dialog?.show()
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    val alert = Dialog(this)
                    alert.ownerActivity = this
                    alert.setCancelable(false)
                    alert.setTitle(getString(R.string.permission_dialog_title))
                    alert.message.text = getString(R.string.permission_needed_alert)
                    alert.create()
                    alert.show()
                } else {
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), MY_PERMISSION_REQUEST_LOCATION)
                }
            } else {
                eventObservable = Single.just<BaseEvent<*>>(GetLastLocation(LocationServices
                        .getFusedLocationProviderClient(this).lastLocation))
                        .doOnSuccess { Log.d("GetLastLocation", FIRED) }.toObservable()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    postOnResumeEvents.onNext(GetLastLocation(LocationServices
                            .getFusedLocationProviderClient(this).lastLocation))
                    Log.d("GetLastLocation", FIRED)
                } else {
                    val dialog = Dialog(this)
                    dialog.ownerActivity = this
                    dialog.setCancelable(false)
                    dialog.setTitle("Location Permission")
                    dialog.message.text = getString(R.string.premission_denied_alert)
                    dialog.create()
                    dialog.show()
                }
                return
            }
            else -> {

            }
        }
    }

    override fun setupUI(isNew: Boolean) {
        setContentView(R.layout.activity_place_list)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Browse Places To Work"
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorFont))
        twoPane = place_detail_container != null
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        placesAdapter = object : GenericRecyclerViewAdapter(
                getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericRecyclerViewAdapter.GenericViewHolder<*> {
                return when (viewType) {
                    R.layout.empty_view -> EmptyViewHolder(layoutInflater
                            .inflate(R.layout.empty_view, parent, false))
                    R.layout.place_item_layout -> PlaceViewHolder(layoutInflater
                            .inflate(R.layout.place_item_layout, parent, false))
                    else -> throw IllegalArgumentException("Could not find view of type $viewType")
                }
            }
        }
        placesAdapter.setAreItemsClickable(true)
        placesAdapter.setOnItemClickListener { _, itemInfo, holder ->
            if (itemInfo.getData<Any>() is ResultsItem) {
                val placeModel = itemInfo.getData<ResultsItem>()
                val placeDetailState = PlaceDetailState(twoPane, placeModel.placeId!!,
                        photoRef = placeModel.photos?.get(0)?.photoReference!!)
                var secondPair: Pair<View, String>? = null
                if (hasLollipop()) {
                    val placeViewHolder = holder as PlaceViewHolder
                    secondPair = Pair.create(placeViewHolder.itemView.title,
                            placeViewHolder.itemView.title.transitionName)
                }
                if (twoPane) {
                    if (currentFragTag!!.isNotEmpty()) {
                        removeFragment(currentFragTag!!)
                    }
                    val placeDetailFragment = PlaceDetailFragment.newInstance(placeDetailState)
                    currentFragTag = placeDetailFragment.javaClass.simpleName + placeModel.id
                    addFragment(R.id.place_detail_container, placeDetailFragment, currentFragTag,
                            secondPair)
                } else {
                    if (hasLollipop()) {
                        val options = ActivityOptions.makeSceneTransitionAnimation(this,
                                secondPair)
                        navigator.navigateTo(this, PlaceDetailActivity.getCallingIntent(this,
                                placeDetailState), options)
                    } else {
                        navigator.navigateTo(this, PlaceDetailActivity.getCallingIntent(this,
                                placeDetailState))
                    }
                }
            }
        }
        place_list.layoutManager = LinearLayoutManager(this)
        place_list.adapter = placesAdapter
    }

    override fun events(): Observable<BaseEvent<*>> {
        return eventObservable.mergeWith(postOnResumeEvents)
    }

    override fun renderSuccessState(successState: PlaceListSate) {
        placesAdapter.dataList = successState.placeList
        successState.diffCallback.dispatchUpdatesTo(placesAdapter)
    }

    override fun toggleViews(toggle: Boolean) {
        loaderLayout.bringToFront()
        loaderLayout.visibility = if (toggle) View.VISIBLE else View.GONE
    }

    @SuppressLint("MissingPermission")
    override fun showError(errorMessage: String) {
        place_list.showErrorSnackBarWithAction(errorMessage, "retry", View.OnClickListener {
            postOnResumeEvents.onNext(GetLastLocation(LocationServices
                    .getFusedLocationProviderClient(this).lastLocation))
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_booking, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.menu_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = "El Born"
        searchView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.darker_gray))
        return super.onCreateOptionsMenu(menu)
    }

    companion object {

        const val MY_PERMISSION_REQUEST_LOCATION = 1

        fun getCallingIntent(context: Context): Intent {
            return Intent(context, PlaceListActivity::class.java)
        }
    }
}
