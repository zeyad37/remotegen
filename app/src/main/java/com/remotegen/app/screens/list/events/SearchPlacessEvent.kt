package com.remotegen.app.screens.list.events

import com.zeyad.rxredux.core.redux.BaseEvent

/**
 * @author by ZIaDo on 4/20/17.
 */
class SearchPlacessEvent(private val query: String) : BaseEvent<String> {

    override fun getPayLoad(): String {
        return query
    }
}
