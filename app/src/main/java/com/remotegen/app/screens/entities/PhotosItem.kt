package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotosItem(@field:SerializedName("photo_reference")
                      val photoReference: String? = null,
                      @field:SerializedName("width")
                      val width: Int? = null,
//                      @field:SerializedName("html_attributions")
//                      val htmlAttributions: List<Any>? = null,
                      @field:SerializedName("height")
                      val height: Int? = null) : Parcelable