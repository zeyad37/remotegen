package com.remotegen.app.screens.detail

import android.view.View
import com.bumptech.glide.Glide
import com.remotegen.app.extensions.millisecstoDateString
import com.remotegen.app.screens.entities.ReviewsItem
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import kotlinx.android.synthetic.main.review_item_layout.view.*

/**
 * @author ZIaDo on 4/9/18.
 */
class ReviewViewHolder(itemView: View) : GenericRecyclerViewAdapter.GenericViewHolder<ReviewsItem>(itemView) {

    override fun bindData(review: ReviewsItem, isItemSelected: Boolean, position: Int, isEnabled: Boolean) {
        Glide.with(itemView.context)
                .load(review.profilePhotoUrl)
                .into(itemView.iv_review_photo)
        itemView.tv_name.text = review.authorName
        itemView.tv_date.text = review.time?.millisecstoDateString()
        itemView.tv_item_review.text = review.rating.toString()
        itemView.tv_review.text = review.text
    }
}


