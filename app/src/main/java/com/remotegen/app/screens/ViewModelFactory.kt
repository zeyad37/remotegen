package com.remotegen.app.screens

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.remotegen.app.screens.detail.PlaceDetailVM
import com.remotegen.app.screens.list.PlaceVM
import com.remotegen.app.screens.review.ReviewVM
import com.zeyad.usecases.api.IDataService

/**
 * @author ZIaDo on 12/13/17.
 */
class ViewModelFactory(private val dataService: IDataService, private val apiKey: String) :
        ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlaceVM::class.java)) {
            return PlaceVM(dataService, apiKey) as T
        }
        if (modelClass.isAssignableFrom(PlaceDetailVM::class.java)) {
            return PlaceDetailVM(dataService, apiKey) as T
        }
        if (modelClass.isAssignableFrom(ReviewVM::class.java)) {
            return ReviewVM(dataService) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.simpleName)
    }
}
