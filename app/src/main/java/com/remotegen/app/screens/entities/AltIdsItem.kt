package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AltIdsItem(@field:SerializedName("scope")
                      val scope: String? = null,
                      @field:SerializedName("place_id")
                      val placeId: String? = null) : Parcelable