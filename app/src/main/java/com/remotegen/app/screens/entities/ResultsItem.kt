package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResultsItem(@field:SerializedName("reference")
                       val reference: String? = null,
                       @field:SerializedName("types")
                       val types: List<String?>? = null,
                       @field:SerializedName("alt_ids")
                       val altIds: List<AltIdsItem?>? = null,
                       @field:SerializedName("scope")
                       val scope: String? = null,
                       @field:SerializedName("icon")
                       val icon: String? = null,
                       @field:SerializedName("name")
                       val name: String? = null,
                       @field:SerializedName("opening_hours")
                       val openingHours: OpeningHours? = null,
                       @field:SerializedName("geometry")
                       val geometry: Geometry? = null,
                       @field:SerializedName("vicinity")
                       val vicinity: String? = null,
                       @field:SerializedName("id")
                       val id: String? = null,
                       @field:SerializedName("photos")
                       val photos: List<PhotosItem?>? = null,
                       @field:SerializedName("place_id")
                       val placeId: String? = null) : Parcelable