package com.remotegen.app.screens

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.remotegen.app.screens.list.PlaceListActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(PlaceListActivity.getCallingIntent(this))
        finish()
    }
}
