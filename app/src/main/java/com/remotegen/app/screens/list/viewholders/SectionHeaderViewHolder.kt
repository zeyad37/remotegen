package com.remotegen.app.screens.list.viewholders

import android.view.View
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import kotlinx.android.synthetic.main.section_header_layout.view.*

/**
 * @author by ZIaDo on 7/18/17.
 */

class SectionHeaderViewHolder(itemView: View) : GenericRecyclerViewAdapter.GenericViewHolder<String>(itemView) {

    override fun bindData(title: String, isItemSelected: Boolean, position: Int, isEnabled: Boolean) {
        itemView.sectionHeader.text = title
    }
}
