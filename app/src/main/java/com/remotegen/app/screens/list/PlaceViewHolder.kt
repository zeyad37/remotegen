package com.remotegen.app.screens.list

import android.view.View
import com.bumptech.glide.Glide
import com.remotegen.app.Constants.PHOTO
import com.remotegen.app.R
import com.remotegen.app.screens.entities.ResultsItem
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import kotlinx.android.synthetic.main.place_item_layout.view.*

/**
 * @author ZIaDo on 4/9/18.
 */
class PlaceViewHolder(itemView: View) : GenericRecyclerViewAdapter.GenericViewHolder<ResultsItem>(itemView) {

    override fun bindData(place: ResultsItem, isItemSelected: Boolean, position: Int, isEnabled: Boolean) {
        itemView.avatar.viewTreeObserver.addOnPreDrawListener {
            Glide.with(itemView.context)
                    .load(PHOTO + place.photos?.get(0)?.photoReference + "&key=${itemView
                            .context.getString(R.string.place_api_key)}&maxwidth=${itemView.avatar.measuredWidth}")
                    .into(itemView.avatar)
            true
        }
        itemView.title.text = place.name
    }
}
