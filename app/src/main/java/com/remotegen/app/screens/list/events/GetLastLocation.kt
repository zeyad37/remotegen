package com.remotegen.app.screens.list.events

import android.location.Location
import com.google.android.gms.tasks.Task
import com.zeyad.rxredux.core.redux.BaseEvent

/**
 * @author ZIaDo on 4/29/18.
 */
class GetLastLocation(private val task: Task<Location>) : BaseEvent<Task<Location>> {
    override fun getPayLoad(): Task<Location> {
        return task
    }
}