package com.remotegen.app.screens.review

import android.os.Parcelable
import com.remotegen.app.screens.entities.Result
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReviewState(val result: Result, val done: Boolean) : Parcelable