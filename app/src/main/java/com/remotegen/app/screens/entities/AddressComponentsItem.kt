package com.remotegen.app.screens.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressComponentsItem(@field:SerializedName("types")
                                 val types: List<String?>? = null,
                                 @field:SerializedName("short_name")
                                 val shortName: String? = null,
                                 @field:SerializedName("long_name")
                                 val longName: String? = null) : Parcelable