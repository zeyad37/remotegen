package com.remotegen.app.ui

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent

/**
 * @author ZIaDo on 1/9/18.
 */
class ScrollEventCalculator {
    companion object {
        /**
         * Determine if the scroll event at the end of the recycler view.
         *
         * @return true if at end of linear list recycler view, false otherwise.
         */
        fun isAtScrollEnd(recyclerViewScrollEvent: RecyclerViewScrollEvent): Boolean {
            val layoutManager = recyclerViewScrollEvent.view().layoutManager
            return if (layoutManager is LinearLayoutManager) {
                layoutManager.itemCount <= layoutManager.findLastVisibleItemPosition() + 2
            } else false
        }
    }
}
