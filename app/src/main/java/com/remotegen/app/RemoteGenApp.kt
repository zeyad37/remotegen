package com.remotegen.app

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.util.Log
import com.zeyad.usecases.api.DataServiceConfig
import com.zeyad.usecases.api.DataServiceFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


/**
 * @author ZIaDo on 4/10/18.
 */
class RemoteGenApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DataServiceFactory.init(DataServiceConfig.Builder(applicationContext)
                .baseUrl("https://api.github.com/")
                .okHttpBuilder(getOkHttpBuilder())
                .postExecutionThread(AndroidSchedulers.mainThread())
                .build())
    }

    private fun getOkHttpBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor { message -> Log.d("NetworkInfo", message) }
                        .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }
}