package com.remotegen.app

/**
 * @author ZIaDo on 6/11/18.
 */
object Constants {
    const val BASE = "https://maps.googleapis.com/maps/api/place/"
    const val SEARCH = BASE + "nearbysearch/json?"
    const val DETAIL = BASE + "details/json?"
    const val PHOTO = BASE + "photo?photoreference="

    const val FIRED = "fired!"
    const val RC_SIGN_IN = 123
    const val IS_SIGNED_IN = "IsSignedIn"
    const val USER_NAME = "UserName"

}