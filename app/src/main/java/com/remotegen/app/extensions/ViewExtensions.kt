package com.remotegen.app.extensions

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Pair
import android.view.View
import android.widget.Toast
import com.andrognito.flashbar.Flashbar
import com.andrognito.flashbar.anim.FlashAnim
import com.dd.template.snackbar.SnackBarFactory
import com.remotegen.app.R
import com.zeyad.rxredux.core.redux.prelollipop.BaseActivity
import java.util.*

/**
 * @author ZIaDo on 3/20/18.
 */
fun BaseActivity<*, *>.addFragment(containerViewId: Int, fragment: Fragment, currentFragTag: String?,
                                   vararg sharedElements: Pair<View, String>?) {
    val fragmentTransaction = this.getSupportFragmentManager().beginTransaction()
    for (pair in sharedElements) {
        fragmentTransaction.addSharedElement(pair?.first, pair?.second)
    }
    if (currentFragTag == null || currentFragTag.isEmpty()) {
        fragmentTransaction.addToBackStack(fragment.tag)
    } else {
        fragmentTransaction.addToBackStack(currentFragTag)
    }
    fragmentTransaction.add(containerViewId, fragment, fragment.tag).commit()
}

fun BaseActivity<*, *>.removeFragment(tag: String) {
    this.getSupportFragmentManager().beginTransaction().remove(this.getSupportFragmentManager().findFragmentByTag(tag))
            .commit()
}

@JvmOverloads
fun Context.showToastMessage(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}

/**
 * Shows a [android.support.design.widget.Snackbar] message.
 *
 * @param message An string representing a message to be shown.
 */
fun AppCompatActivity.showFlashBarMessage(message: String, duration: Long = 500): Flashbar.Builder {
    return Flashbar.Builder(this)
            .gravity(Flashbar.Gravity.BOTTOM)
            .duration(duration)
            .message(message)
            .showOverlay()
            .enterAnimation(FlashAnim.with(this)
                    .animateBar()
                    .duration(750)
                    .alpha()
                    .overshoot())
            .exitAnimation(FlashAnim.with(this)
                    .animateBar()
                    .duration(400)
                    .accelerateDecelerate())
            .icon(R.drawable.ic_done)
            .iconColorFilterRes(R.color.colorAccent)
            .iconAnimation(FlashAnim.with(this)
                    .animateIcon()
                    .pulse()
                    .alpha()
                    .duration(750)
                    .accelerate())
            .enableSwipeToDismiss()
}

fun View.showSnackBarWithAction(typeSnackBar: String, message: String, actionText: String,
                                onClickListener: View.OnClickListener) {
    SnackBarFactory
            .getSnackBarWithAction(typeSnackBar, this, message, actionText, onClickListener)!!.show()
}

fun View.showSnackBarWithAction(typeSnackBar: String, message: String, actionText: Int,
                                onClickListener: View.OnClickListener) {
    this.showSnackBarWithAction(typeSnackBar, message, this.context.getString(actionText),
            onClickListener)
}

/**
 * Shows a [android.support.design.widget.Snackbar] errorResult message.
 *
 * @param message  An string representing a message to be shown.
 * @param duration Visibility duration.
 */
fun View.showErrorSnackBar(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    SnackBarFactory.getSnackBar(SnackBarFactory.TYPE_ERROR, this, message, duration)!!.show()
}

fun View.showErrorSnackBarWithAction(message: String, actionText: String,
                                     onClickListener: View.OnClickListener) {
    SnackBarFactory.getSnackBarWithAction(SnackBarFactory.TYPE_ERROR, this, message, actionText,
            onClickListener)!!.show()
}

fun String.millisecstoDateString(): String {
    val cl = Calendar.getInstance()
    cl.timeInMillis = this.toLong()  //here your time in miliseconds
    val date = "" + cl.get(Calendar.DAY_OF_MONTH) + "/" + cl.get(Calendar.MONTH) + "/" + cl.get(Calendar.YEAR)
    val time = "" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" + cl.get(Calendar.SECOND)
    return "$date $time"
}

fun Int.millisecstoDateString(): String {
    return this.toString().millisecstoDateString()
}